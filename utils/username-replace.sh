#!/bin/bash

#shellcheck disable=SC2154
#shellcheck disable=SC2086
#shellcheck disable=SC2044

print_help() {
  cat << EOF

This command replaces the name of solution files and evidence folders made by
users whose username changed.

It takes the following inputs:
./replace.sh [path-to-repo] [old-username] [new-username]

Example:
./replace.sh /home/dsalazar/writeups dsalazaratfluid podany270895

Output:
All feature files and evidences with name dsalazaratfluid will be renamed to
podany270895
EOF
}

if [[ -z $2 || -z $3 ]]; then
  echo "Error: You must provide old and new usernames"
  print_help
  exit 1
fi

if [[ ! -d $1 ]]; then
  echo "Error: Invalid repo directory. Please provide path to training repo."
  print_help
  exit 1
fi

cd "$1" || exit 1
for feature in $(find ./code/ ./hack/ -mindepth 3 -iname "$2*"); do
   mv $feature ${feature/$2/$3}
done

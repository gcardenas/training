object JuanDavidRobles339A {
  def main(args: Array[String]): Unit = {

    import scala.io.StdIn

    val numbers: Array[String] = Array("1", "2", "3")
    var string: String = StdIn.readLine()

    var j: Int = 0
    var index: Int = 0
    var output: String = ""
    var str1: String = ""
    var str2: String = ""

    for (i <- 0 until 3){

      while (string contains numbers(i)){
        index = string indexOf numbers(i)
        if (index > 0) {
          str1 = string.substring(0, index)
        } else {
          str1 = ""
        }
        if (index < string.size-1) {
          str2 = string.substring(index + 1)
        } else {
          str2 = ""
        }
        string = str1 + str2
        output = output + numbers(i) + "+"
      }
    }

    output = output.substring(0, output.size-1)

    println(output)
  }
}

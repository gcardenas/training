import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Codeforces - 2A.
 *
 * @author JuanDavidRobles
 * @see JuanDavidRobles
 */

final class JuanDavidRobles {

    /**
     * Empty constructor.
     */
    private JuanDavidRobles() {
    }

    /**
     * Find whether there is an String in a List of Strings.
     *
     * @param names List where to search
     * @param name String to search on the List
     * @return int index of string if this is present, if no returns -1
     */
    private static int searchName(final List<String> names, final String name) {
        for (int i = 0; i < names.size(); i++) {
            if (names.get(i) != null && names.get(i).equals(name)) {
                return i;
            }
        }
        return -1;
    }


    /**
     * Find the Integers of a List that are equals to a such value.
     *
     * @param scores List where to search
     * @param maxValue int The value of compation
     * @param posMaxList List to refresh with the new values
     * @return List with the index of the values that are equals to the
     * comparison value
     */

    private static List<Integer> findMax(final List<Integer> scores,
                                         final int maxValue,
                                         final List<Integer> posMaxList) {
        int j = 0;

        for (int i = 0; i < scores.size(); i++) {
            if (scores.get(i) == maxValue) {
                if (!posMaxList.contains(i))  {
                    posMaxList.add(i);
                }
            } else  {
                if (posMaxList.contains(i)) {
                    posMaxList.remove(posMaxList.indexOf(i));
                }
            }
        }

        return posMaxList;
    }

    /**
     * Find the max value of a List of integers.
     *
     * @param scores List where to find the max value
     * @return int max value in the List
     */

    private static int findMaxValue(final List<Integer> scores) {
        int maxValue = scores.get(0);
        for (int i = 0; i < scores.size(); i++) {
            if (scores.get(i) > maxValue) {
                maxValue = scores.get(i);
            }
        }
        return maxValue;
    }

    /**
     * main function.
     *
     * @param args args
     */

    public static void main(final String[] args)  {
        InputStreamReader streamReader = new InputStreamReader(System.in);
        Scanner scanner = new Scanner(new BufferedReader(streamReader));

        List<Integer> posMaxList = new ArrayList<>();

        int n = Integer.parseInt(scanner.next());

        List<String> names = new ArrayList<>();
        List<Integer> scores = new ArrayList<>();

        List<String> maxHistoryPlayers = new ArrayList<>();
        List<Integer> maxHistoryValues = new ArrayList<>();
        int playerNumbers = 0;
        boolean flag = false;

        //Variables de ciclo temporales
        String name;
        int score;
        int pos;

        int maxValue = 0;

        for (int i = 0; i < n; i++) {
            name = scanner.next();
            score = Integer.parseInt(scanner.next());

            pos = searchName(names, name);
            if (pos == -1) {
                names.add(name);
                scores.add(score);
                playerNumbers++;
            } else  {
                scores.set(pos, scores.get(pos) + score);
            }

            maxValue = findMaxValue(scores);

            posMaxList = findMax(scores, maxValue, posMaxList);

            maxHistoryValues.add(maxValue);
            maxHistoryPlayers.add(names.get(scores.indexOf(maxValue)));
        }

        if (posMaxList.size() == 1)  {

            System.out.println(names.get(posMaxList.get(0)));
        } else  {
            for (int i = 0; i < maxHistoryValues.size(); i++) {
                if (maxHistoryValues.get(i) >= maxValue) {

                    for (int j = 0; j < posMaxList.size(); j++) {
                        if (maxHistoryPlayers.get(i).equals(names.
                                get(posMaxList.get(j)))) {
                            System.out.println(names.get(posMaxList.get(j)));
                            flag = true;
                        }
                    }

                    if (flag) {
                        break;
                    }
                }
            }
        }

    }
}

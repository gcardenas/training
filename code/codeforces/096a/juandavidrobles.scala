object JuanDavidRobles96A {
  def main(args: Array[String]): Unit = {

    import scala.io.StdIn

    val line : String = StdIn.readLine()

    if ((line contains "1111111") || (line contains "0000000")){
      println("YES")
    } else {
      println("NO")
    }
  }
}

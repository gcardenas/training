'''
$ pylint karlhanso82.py #linting
No config file found, using default configuration
--------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
$ python karlhanso82.py #compilation
'''
__version__ = "0.6"

import configparser
import os

CONFIG_PARSER = configparser.ConfigParser()
CONFIG_PARSER.read(os.path.join(os.getcwd(), 'DATA.lst'))
A = int(CONFIG_PARSER.get('value', 'val.a1'))
B = int(CONFIG_PARSER.get('value', 'val.b1'))
COUNT = int(CONFIG_PARSER.get('value', 'val.i'))

for c in bin(COUNT):
    (A, B) = (int(CONFIG_PARSER.get('value', 'val.dos')) * A * B - A * A, A
              * A + B * B)
    if CONFIG_PARSER.get('value', 'val.b1') == c:
        (A, B) = (B, A + B)
    while A >> int(CONFIG_PARSER.get('value', 'val.up')):
        A //= int(CONFIG_PARSER.get('value', 'val.ten'))
        B //= int(CONFIG_PARSER.get('value', 'val.ten'))
print A

# pylint: disable=pointless-string-statement
'''
$ python karlhanso82.py
4
87 73 64 61
46 37 53 58 70
44 48 51 56 32
other A#-minor G#-major
'''

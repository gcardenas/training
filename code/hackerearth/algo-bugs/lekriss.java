/*
$ Checkstyle lekriss.java #linting
$ javac lekriss.java #Compilation
*/
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;

public class lekriss {

  public static void main (String[] args){
    FileReader in = null;
    BufferedReader input = null;
    int numOfTasks = 0;
    ArrayList<Integer> container = null;

    try{
      in = new FileReader("DATA.lst");
      input = new BufferedReader(in);
      numOfTasks = Integer.parseInt(input.readLine());
      container = new ArrayList<Integer>();

      for(int i=0; i<numOfTasks; i++) {
        String[] temp = input.readLine().split(" ");
        int control = Integer.parseInt(temp[0]);
        if(control==1) {
          container.add(Integer.parseInt(temp[1]));
        }
        if(control==2) {
          Collections.sort(container, Collections.reverseOrder());
          if(container.size()<3) {
            System.out.println("Not enough enemies");
          }
          else {
            int result = container.get(container.size()/3-1);
            System.out.println(result);
          }
        }
      }
    }
    catch (Exception e) {
        e.printStackTrace();
    }
  }
}
/*
$ java lekriss
$ Not enough enemies
$ 9
$ 9
*/

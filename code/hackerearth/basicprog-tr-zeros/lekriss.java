/*
$ Checkstyle lekriss.java #linting
$ javac lekriss.java #Compilation
*/
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Scanner;

class lekriss {

  public static void main (String args[]) throws Exception{

    File file = new File("DATA.lst");
    FileReader r = new FileReader(file);
    BufferedReader br = new BufferedReader(r);
    Scanner input = new Scanner(br);
    int testCases = input.nextInt();
    for(int i=0; i<testCases; i++) {
      int factToFind = input.nextInt();
      String result = "";
      if(determineStartingPoint(factToFind)!=0) {
        int iterator = determineStartingPoint(factToFind);
        int counter = 0;
        while(numOfZeros(iterator)==factToFind) {
          result += iterator + " ";
          iterator++;
          counter++;
        }
        System.out.println(counter);
        System.out.println(result);
      }
      else System.out.println(0);
    }
  }

  public static int numOfZeros(int numberToTest) {
    int counter = 0;
    while(numberToTest/5>=1) {
      counter+=numberToTest/5;
      numberToTest/=5;
    }
    return counter;
  }

  public static int determineStartingPoint(int trailingZeros) {
    int i = 0;
    boolean checker = false;
    while(!checker) {
      if(numOfZeros(i)==trailingZeros) {
        checker = !checker;
        return i;
      }
      else i+=5;
      if(numOfZeros(i)>trailingZeros)
        break;
    }
    return i;
  }
}
/*
$ java lekriss
$ 5
$ 5 6 7 8 9
*/

﻿//Suma y diferencia o producto y división de dos números

import java.util.Scanner;

public class Reto1 {
    public static void main(String[] args) {
  
        Scanner digit      = new Scanner(System.in);
        int numeroUno      = 0;
        int numeroDos      = 0;
        int suma           = 0;
        int diferencia     = 0;
        int multiplicacion = 0;
        int division       = 0;
        
        System.out.println("");
        System.out.println("*******");
        System.out.println("*Reto1*");
        System.out.println("*******");
        System.out.println("");

        System.out.print("Ingrese el primer numero por favor: "); 
        numeroUno = digit.nextInt();
        System.out.println("");
        System.out.print("Ingrese el segundo numero por favor: "); 
        numeroDos = digit.nextInt();
        System.out.println("");

        suma           = numeroUno+numeroDos;
        diferencia     = numeroUno-numeroDos;
        multiplicacion = numeroUno*numeroDos;
        division       = numeroUno/numeroDos;

        if (numeroUno > numeroDos) {
            System.out.println("La suma de los numeros es: "+suma
                               +" y su diferencia es: "+diferencia);
        }
        else {
            System.out.println("La multiplicacion de los numeros es: "
                               +multiplicacion+" y su division es: " 
                               +division);
        }
    }
}

#!/usr/bin/env jsc
/*
$ eslint jicardona.js
*/

/* global arguments, print */

/* eslint-disable fp/no-arguments */
const [ input ] = arguments;
/* eslint-disable fp/no-arguments */

const [ total ] = input.split(/\n/);
const iniPoints =
  input.split(/\n/).slice(1).map((curr) => curr.split(/\s/).map(Number));

const values = total.split(/\s/).slice(1);

function divideSegments(points, alpha, index = 0, newPoints = []) {
  if (index > points.length - 2) {
    return newPoints;
  }
  const xValue =
    points[index][0] +
      ((points[index + 1][0] - points[index][0]) * alpha);
  const yValue =
    points[index][1] +
      ((points[index + 1][1] - points[index][1]) * alpha);
  return divideSegments(
    points, alpha, index + 1, newPoints.concat([ [ xValue, yValue ] ]));
}

function calculatePoints(points, alpha, curve = []) {
  if (alpha >= 1) {
    return curve.concat(iniPoints.slice(iniPoints.length - 1)[0]);
  }
  if (points.length === 1) {
    const newAlpha = alpha + (1 / (values - 1));
    const beizerPoint = points[0].map(Math.round);
    return calculatePoints(iniPoints, newAlpha, curve.concat(beizerPoint));
  }
  const newPoints = divideSegments(points, alpha);
  return calculatePoints(newPoints, alpha, curve);
}

/* eslint-disable fp/no-unused-expression*/
print(calculatePoints(iniPoints, 0).join(' '));
/* eslint-enable fp/no-unused-expression*/


/*
$ jsc jicardona.js -- "`cat DATA.lst`"
717 538 724 498 721 469 711 448 694 432 673 421 649 413 622 407 593 403 565 401
536 400 508 401 481 404 456 408 433 415 412 424 393 434 377 447 364 462 355 479
348 498 345 518 345 539 349 560 357 582 368 604 383 625 402 645 424 663 450 679
479 694 512 706 547 716 585 724 625 730 665 735
*/

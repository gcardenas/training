#!/usr/bin/env python3
""" This script implements the unranking algorithm which finds
the combination at a given position in the combinatorial number system.
"""

def choose(pop, sample):
    """ Computes the number of combinations of sample
    elements taken from pop elements. """
    ret = 1.0
    if sample > pop/2:
        sample = pop - sample
    for i in range(sample):
        ret *= (pop-i)/(sample-i)
    return int(round(ret, 0))

def unrank(pop, sample, pos):
    """ Finds the pos-th combination in lexicographic order. """
    combi = ""
    j = 0
    for i in range(sample):
        while choose(pop-j-1, sample-i-1) <= pos:
            pos -= choose(pop-j-1, sample-i-1)
            j += 1
        combi += SYMBOLS[j]
        j += 1
    return combi

SYMBOLS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
F = open("DATA.lst", "r")
L = int(F.readline())
OUT = ""
for l in range(L):
    nt, kt, pt = list(map(int, F.readline().split()))
    OUT += unrank(nt, kt, pt) + " "
print(OUT)
#
# pylint raballestasr.py 
# No config file found, using default configuration
# ************* Module raballestasr
# C: 35, 0: Final newline missing (missing-final-newline)
#
#------------------------------------------------------------------
#Your code has been rated at 9.60/10 (previous run: 9.62/10, -0.02)

program maracutavowelcount; {Nombre del programa}

uses
  SysUtils;

var
  Num2, x, control, y, sizepalabra: longint; {Declaro variables como enteras}
  palabra: string;
  vector: array [1..100] of longint;
  bandera: string;

begin
  sizepalabra := 0;
  writeLn('Escriba cuantas lineas son:');
  readLn(control);
  Num2 := 0;

  for x := 1 to control do
  begin
    writeLn('Escriba la cadena:');
    readLn(palabra);
    sizepalabra := Length(palabra);
    for y := 1 to sizepalabra do
    begin
      if (palabra[y] = 'a') or (palabra[y] = 'e') or (palabra[y] = 'i') or
        (palabra[y] = 'o') or (palabra[y] = 'u') or (palabra[y] = 'y') then
        Num2 := Num2 + 1;
    end;
    vector[x] := Num2;
    Num2 := 0;
  end;

  for x := 1 to control do
  begin
    Write(vector[x]);
    Write(' ');
  end;

  readLn();

end.

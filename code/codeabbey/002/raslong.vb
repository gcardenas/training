Imports System

Module Program
    Sub Main(args As String())
        Dim n = Console.ReadLine()
        Dim val(n - 1, 1)
        For j As Integer = 0 To n - 1
            Dim Line = Console.ReadLine()
            Dim lspl = Line.Split(" ")
            For k As Integer = 0 To 1
                val(j, k) = lspl(k)
            Next
        Next
        For i As Integer = 0 To n - 1
            Console.Write(Integer.Parse(val(i, 0)) + Integer.Parse(val(i, 1)) & " ")
        Next
        Console.ReadKey(True)
    End Sub
End Module

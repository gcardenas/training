/*
$ rustfmt santiyepes.rs --write-mode=diff
$
$ rustc santiyepes.rs
$
*/
use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
  let file = File::open("DATA.lst").unwrap();

  for buff in BufReader::new(file).lines() {
    let mut lines: &str = &buff.unwrap();
    let mut data: Vec<&str> = lines.split(' ').collect();
    let mut numbers: Vec<i64> = Vec::new();

    for x in &data {
      let mut string_number: String = x.to_string();
      let mut number_conver: i64 = string_number.parse::<i64>().unwrap();
      numbers.push(number_conver);
    }
    if numbers.len() > 1 {
      let num = numbers[0];
      let increment = numbers[1];
      let times = numbers[2];
      let mut result = 0;
      for x in 0..times {
        result += num + x * increment;
      }
      print!("{} ", result);
    }
  }
}

/*
$ ./santiyepes
91180 49039 29762 9860 3552 867 97119 3225 779 3654
*/

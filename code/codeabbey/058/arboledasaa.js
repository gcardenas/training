/*
$ eslint jarboleda.js
$
*/

const suits = [ 'Clubs', 'Spades', 'Diamonds', 'Hearts' ];
const rankNumbers = [ '2', '3', '4', '5', '6', '7', '8', '9', '10' ];
const rankNames = [ 'Jack', 'Queen', 'King', 'Ace' ];
const ranks = rankNumbers.concat(rankNames);

function solveForNumber(cardValue) {
  const suitValue = Math.floor(cardValue / ranks.length);
  const rankValue = cardValue % ranks.length;
  const parsedNumber = `${ ranks[rankValue] }-of-${ suits[suitValue] }`;
  return parsedNumber;
}

function solver(mistake, contents) {
  if (mistake) {
    return mistake;
  }
  const splited = contents.split('\n');
  const matrix = splited[1].split(' ');
  const answer = matrix.filter((word) => (word.length > 0)).map((theNumber) =>
    (solveForNumber(theNumber))).join(' ');
  const output = process.stdout.write(`${ answer }\n`);
  return output;
}

const fileReader = require('fs');

function fileLoad() {
  return fileReader.readFile('DATA.lst', 'utf8', (mistake, contents) =>
    solver(mistake, contents)
  );
}

/* eslint-disable fp/no-unused-expression*/
fileLoad();

/**
$ node jarboleda.js
output:
8-of-Clubs
 Queen-of-Spades
 9-of-Diamonds
 3-of-Diamonds
 Jack-of-Spades
 2-of-Diamonds
 8-of-Hearts
 8-of-Diamonds
 7-of-Diamonds
 Jack-of-Diamonds
 Jack-of-Hearts
 4-of-Spades
 4-of-Diamonds
 5-of-Diamonds
 Queen-of-Hearts
 King-of-Hearts
 6-of-Diamonds
 4-of-Hearts
 Jack-of-Clubs
 10-of-Diamonds
 3-of-Spades
 Ace-of-Clubs
 Queen-of-Diamonds
 King-of-Clubs
 10-of-Spades
 Ace-of-Hearts
 2-of-Spades
 6-of-Clubs

*/

"""
  Feature: Solving the challenge #138 Huffman Coding
  With Python v3
  From http://www.codeabbey.com/index/task_view/huffman-coding


 Linting:   pylint jenniferagve.py
    --------------------------------------------------------------------
    Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)

"""
import functools


def letter_counter():
    """Calculate the number of times that a letter is repeat, sorted them
       by value and count the original size"""
    data = open('DATA.lst', "r")
    data = data.readline()
    uni_letters = list(set(data))
    letter_count = map(data.count, uni_letters)
    list1 = map(None, uni_letters, letter_count)
    list1 = map(list, list1)
    list1 = sorted(list1, key=lambda x: (-x[1], x[0]))
    original_size = float(len(data) * 8)
    return list1, original_size


def get_position(list1):
    """Add the value of the last two letters or array of letters
     and reorder them in the tree depending on the value of the sum"""
    value1 = list1.pop()
    value2 = list1.pop()
    value3 = value1[1] + value2[1]
    if list1:
        count = [item[1] for item in list1]
        count = list(count)
        location = [x >= value3 for x in count]
        if False in location:
            number = location.index(False)
            list1.insert(number, [[value2[0], value1[0]], value3])
        else:
            list1.append([[value2[0], value1[0]], value3])
    else:
        list1.append([[value2[0], value1[0]], value3])
    return list1


def binary_code(list1, number_sel, array1):
    """put the 1 or O depending on the side of the tree where is located
        the letter"""
    indicator = number_sel
    if len(list1) != 1:
        direction = list1[0]
        number_sel += "1"
        binary_code(direction, number_sel, array1)
        direction = list1[1]
        number_sel = indicator
        number_sel += "0"
        binary_code(direction, number_sel, array1)
        return array1
    array1.append([list1, number_sel])
    return True


def final_size(final, letter_count):
    """Calculate the compressed size of the text based on the number of its
    binary representation"""
    final.sort()
    binary = [item[1] for item in final]
    binary_len = list(map(len, binary))
    total = [item_l1 * item_l2 for item_l1, item_l2
             in zip(letter_count, binary_len)]
    compressed_size = float(functools.reduce(lambda item1, item2:
                                             item1 + item2, total))
    return compressed_size


def main():
    """Main where each function is called to calculate the Huffman
    coding"""
    list1, original_size = letter_counter()
    letter_count = [item[1] for item in sorted(list1)]
    while len(list1) > 1:
        list1 = get_position(list1)
    list1 = list1[0][0]
    direction = ""
    array1 = []
    final = binary_code(list1, direction, array1)
    compressed_size = final_size(final, letter_count)
    ratio = original_size / compressed_size
    print ratio


main()

# pylint: disable=pointless-string-statement


''' python jenniferagve.py
    input: DAVIDAHUFFMAN
    -------------------------------------------------------------------
    output: 2.6 '''

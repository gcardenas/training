; $ lein check
;   Compiling namespace kedavamaru.clj
; $ lein eastwood
;   == Eastwood 0.2.1 Clojure 1.8.0 JVM 1.8.0_181
;   Directories scanned for source files:
;     src test
;   == Linting kedavamaru.clj ==
;   == Warnings: 0 (not including reflection warnings)  Exceptions thrown: 0

; namespace
(ns kedavamaru.clj
  (:gen-class)
)

; average of vector of integers
(defn avg
  ([arr]
    (let [c (count arr)
          s (apply + arr)
          a (Math/round (double (/ s (- c 1))))]
      a
    )
  )
)

; tokenize whitespace separated string into a vector of integers
(defn stovi
  ([wsstr]
    (let [l (map #(Integer/parseInt %) (clojure.string/split wsstr #" "))
          v (into [] l)]
      v
    )
  )
)

; parse file line by line and solve
(defn process_file
  ([file]
    (with-open [rdr (clojure.java.io/reader file)]
      (doseq [line (line-seq rdr)]
        (let [v (stovi line)]
          (if-not (= 1 (count v))
            (print (str (avg v) " "))
          )
        )
      )
    )
  )
)

; execute
(defn -main
  ([& args]
    (process_file "DATA.lst")
    (println)
  )
)

; $lein run
;   6375 231 421 3489 145 407 209 8453 145 974 451 3330

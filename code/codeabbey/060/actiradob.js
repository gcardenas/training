/*
$ eslint actiradob.js
$
*/

function growTree(tree, index, line) {
  while (index < tree.length) {
    if (typeof tree[index] === 'object') {
      const [ actual, candies ] = tree[index];
      const indexTwo = (index * 2) + 1;
      const indexThree = (index * 2) + 2;
      const three = 3;
      let twoMore = 0;
      if (actual < line.length - 2) {
        twoMore = candies + parseInt(line[actual + 2], 10);
        tree[indexTwo] = [ actual + 2, twoMore ];
      }
      let threeMore = 0;
      if (actual < line.length - three) {
        threeMore = candies + parseInt(line[actual + three], 10);
        tree[indexThree] = [ actual + three, threeMore ];
      }
    }
    index += 1;
  }
  tree = tree.filter((value) => value !== 'undefined');
  tree = tree.map((value) => Math.max(...value));
  return Math.max(...tree);
}

function jumps(line) {
  const arrayTree = [ [ 0, parseInt(line[0], 10) ] ];
  return growTree(arrayTree, 0, line);
}

function rab(miss, file) {
  let lines = file.split(/\r\n|\r|\n/g);
  lines.shift();
  lines = lines.map((value) => value.split(' '));
  let candies = lines.map((line) => jumps(line));
  candies = candies.join(' ');
  process.stdout.write(candies);
}

const fileS = require('fs');

function fileLoad() {
  return fileS.readFile('DATA.lst', 'utf8', (miss, file) => rab(miss, file));
}

fileLoad();

/*
$ node actiradob.js
193 142 219 ... ... 122 126 209
*/

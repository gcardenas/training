"""
codeabbey 85 - rotation in 2d space

$ pylint podany270895.py
No config file found, using default configuration

--------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
"""


from math import radians, sin, cos


def rotate(xcoord, ycoord, angle):
    """
    This function rotates a vector (xcoord,ycoord) by an angle
    """
    angle = radians(angle)
    newxcoord = int(xcoord*cos(angle) - ycoord*sin(angle))
    newycoord = int(xcoord*sin(angle) + ycoord*cos(angle))
    return newxcoord, newycoord


def read_file(path):
    """
    Parses the input file and stores the information in variables
    stars_number, angle and stars matrix
    """
    input_file = open(path).read().split()
    stars_number = int(input_file[0])
    angle = int(input_file[1])
    stars = []
    input_file = input_file[2::]
    for i in xrange(0, stars_number*3, 3):
        name = input_file[i]
        xcoord = input_file[i+1]
        ycoord = input_file[i+2]
        stars.append([name, xcoord, ycoord])
    return stars, angle


def rotate_stars(stars, angle):
    """
    Rotates all the stars from one array
    """
    for star in stars:
        star[1] = int(star[1])
        star[2] = int(star[2])
        star[1], star[2] = rotate(star[1], star[2], angle)
    return stars


def sort_stars(stars):
    """
    Sorts the stars arrays using y as first criteria and x as second criteria
    and returns it
    """
    return sorted(stars, key=lambda x: (x[2], x[1]))


def main():
    """
    opens the input file, calls the relevant functions and creates the answer
    string
    """
    stars, angle = read_file("DATA.lst")
    stars = rotate_stars(stars, angle)
    stars = sort_stars(stars)
    answer = ""
    for star in stars:
        answer = answer + star[0] + " "
    print answer


main()

# pylint: disable=pointless-string-statement

"""
$ python podany270895.py
output:
Diadem Alcor Capella Pherkad Mizar Nembus Algol Rigel Altair Aldebaran \
    Betelgeuse Jabbah Thabit Media Electra Procyon Kochab Vega Mira Albireo \
    Heka Lesath Deneb Castor Fomalhaut
"""

/*
  $ g++ tizcano.cpp -o tizcano.out
  $
*/
#include<iostream>
#include<string>
#include<fstream>
#include<map>
#include<vector>
#include<set>
#include<queue>

using namespace std;

vector<string> words;
map< string, vector<string> > similar_words;
map< string, set<string> > graph;

void build_graph() {
  ifstream input("DATA.lst");
  if (input.is_open()) {
    while(!input.eof()) {
      string word;
      getline(input, word);
      if (!word.empty()) {
        word.erase(word.length()-1);
        words.push_back(word);
      }
    }
  }
  for (string i : words) {
    for (int j = 1; j <= (int)i.size(); ++j) {
      string combination_word = i.substr(0, j - 1) + '_'+ i.substr(j, i.size());
      similar_words[combination_word].push_back(i);
    }
  }
  for (auto const& x : similar_words) {
    for (string i : x.second) {
      for (string j : x.second) {
        if (i != j) {
          graph[i].insert(j);
          graph[j].insert(i);
        }
      }
    }
  }
}
int bfs(string start, string end) {
  map<string, bool> visited;
  queue< pair<string, int> > q;
  vector<string> current_path;
  string temp;
  visited[start] = true;
  q.push(make_pair(start, 1));
  while (!q.empty()) {
    temp = q.front().first;
    int temp_counter = q.front().second;
    if (temp == end) {
      return temp_counter;
    }
    q.pop();
    for (string i : graph[temp]) {
      if (!visited[i]) {
        visited[i] = true;
        q.push(make_pair(i, temp_counter + 1));
      }
    }
  }
  return -1;
}
int main() {
  build_graph();
  int test_cases;
  cin>>test_cases;
  string one, two;
  for (int i = 0; i < test_cases; ++i) {
    cin>>one>>two;
    cout<<min(bfs(one, two), bfs(two, one))<<" ";
  }
  cout<<endl;
}
/*
  $ ./tizcano.out
  input:
  12
  ponds egged
  roots wolfs
  madly roger
  fests feuds
  tales neath
  slept slops
  flout baked
  scowl feral
  rouse harms
  mayor books
  melts tinny
  toast great
  output:
  9 4 11 6 8 7 17 16 8 10 10 11
*/

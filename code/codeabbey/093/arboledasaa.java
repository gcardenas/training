/*
$ javac arboledasaa.java
0
$
*/
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.PriorityQueue;


public class arboledasaa {

  public static void main(String[] args) throws IOException {
    // pre
    /// variable declaration
    //// total amount of visitors N and the seed for random generator X0
    int n, x0;
    /*
     * the input stream
     */
    BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    /*
     * a temporal variables
     */
    String line;
    String array[];
    // queue
    PriorityQueue<Beggar> beggars = new PriorityQueue<>();
    // total
    long totalDiscomfort = 0;
    // input
    line = in.readLine();
    array = line.split(" ");
    n = Integer.parseInt(array[0]);
    x0 = Integer.parseInt(array[1]);
    in.close();
    /// Create a stream of randoms
    LinearCongruentialGenerator lcg = new LinearCongruentialGenerator(445, x0,
        700001, 2097152);
    // add the initial beggar to the queue
    beggars.add(new Beggar(0, getStarvationDegree(lcg.getXNext())));
    // processing
    // Simulate the machine by steps, while there are visitors
    for (int t = 1; t < n || !beggars.isEmpty(); t++) {
      if (t % 2 == 0) {
        Beggar beggar = beggars.poll();
        long personalDiscomfort = beggar.getStarvationDegree()
            * (t - beggar.getArrivalTime());
        totalDiscomfort += personalDiscomfort;
      }
      if (t < n) {
        beggars.add(new Beggar(t, getStarvationDegree(lcg.getXNext())));
      }
    }
    // output
    System.out.println(totalDiscomfort);
    // post
    System.exit(0);
  }

  private static int getStarvationDegree(int rnd) {
    return (rnd % 999) + 1;
  }

}

class LinearCongruentialGenerator {
  private long a, xCur, c, m;

  public LinearCongruentialGenerator(int a, int x0, int c, int m) {
    this.a = a;
    this.xCur = x0;
    this.c = c;
    this.m = m;
  }

  public int getXNext() {
    long xNext = (a * xCur + c) % m;
    this.xCur = xNext;
    return (int) xNext;
  }
}

class Beggar implements Comparable<Beggar> {
  private int starvationDegree, arrivalTime;

  public Beggar(int arrivalTime, int starvationDegree) {
    this.arrivalTime = arrivalTime;
    this.starvationDegree = starvationDegree;
  }

  @Override
  public boolean equals(Object obj) {
    try {
      Beggar other = (Beggar) obj;
      return other.starvationDegree == this.starvationDegree
          && this.arrivalTime == other.arrivalTime;
    } catch (Exception exception) {
      return false;
    }
  }

  public int getStarvationDegree() {
    return starvationDegree;
  }

  public int getArrivalTime() {
    return arrivalTime;
  }

  @Override
  public int compareTo(Beggar o) {
    if (this.starvationDegree > o.starvationDegree) {
      return -1;
    } else if (this.starvationDegree < o.starvationDegree) {
      return 1;
    } else {
      return o.arrivalTime - this.arrivalTime;
    }
  }

}
/*
$ cat DATA.lst | java arboledasaa
11153064781
 */

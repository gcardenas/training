/*
$ go lint vmelendez.go
$ go run vmelendez.go
*/
package main

import (
  "bufio"
  "fmt"
  "os"
  "strconv"
  "strings"
)

func main() {
  f, err := os.Open("DATA.lst")
  if err != nil {
    fmt.Printf("error opening file: %v\n", err)
    os.Exit(1)
  }

  var x [100]string
  scanner := bufio.NewScanner(f)
  i := 0
  for scanner.Scan() {
    x[i] = scanner.Text()
    i++
  }

  n, _ := strconv.Atoi(x[0])
  var num [100]int
  var str [100]string
  for i := 0; i < n; i++ {
    s := strings.Split(x[i+1], " ")
    str[i] = s[1]
    num[i], _ = strconv.Atoi(s[0])
  }

  for i := 0; i < n; i++ {
    if num[i] < 0 {
      num[i] = len(str[i]) + num[i]
    }
    for j := num[i]; j < len(str[i]); j++ {
      fmt.Printf("%c", str[i][j])
    }
    for k := 0; k < num[i]; k++ {
      fmt.Printf("%c", str[i][k])
    }
    fmt.Printf(" ")
  }
}
/*
$ go run vmelendez
whomthebelltollsfor numberverycomplex
*/

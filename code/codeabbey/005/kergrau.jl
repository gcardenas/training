#=
 $julia
 julia> using Lint
 julia> lintfile("kergrau.jl")
=#

open("DATA.lst") do file
  flag = false
  answer = ""

  for ln in eachline(file)
    if flag == false
      flag = true
      continue
    end

    i = split(ln, " ")
    number1 = parse(Int64, i[1])
    number2 = parse(Int64, i[2])
    number3 = parse(Int64, i[3])
    println(minimum([number1, number2, number3]))

  end
end

# $julia kergrau.jl
# 107814 386086 -5721210 -3265497 -9006366 124682 -455274 -4695046 -2027624
# -2726599 6289004 -2232700 -5564619 -9844812 -3110310 -2335335 -9253696
# -9053831 -3981282 -2784460 -1406274 -8164135 3309434

open System

[<EntryPoint>]
let main argv =
    let Operations a b c =
        let mutable num = a*b+c
        let mutable sum = 0
        while (num > 0 ) do
            sum <- sum + (num % 10)
            num <- int (num/10)
        sum
    printfn "ingrese la cantidad de pruebas:"
    let c = stdin.ReadLine() |> int
    let mutable out = Array.zeroCreate c
    for i in 0..(c-1) do
        printfn "(%i) ingrese tres numeros separados por un espacio"(i+1)
        let dataArray = stdin.ReadLine().Split ' '
        let x = int dataArray.[0]
        let y = int dataArray.[1]
        let z = int dataArray.[2]
        out.[i] <- Operations x y z
    for i in 0..(out.Length-1) do
        printf "%i "out.[i]
    Console.ReadKey() |> ignore
    0 // devolver un codigo de salida entero

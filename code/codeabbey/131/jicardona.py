#!/usr/bin/env python2.7
"""
$ pylint jicardona.py
No config file found, using default configuration

------------------------------------
Your code has been rated at 10.00/10
"""

TEST_CASES = []

with open('DATA.lst') as data:
    TOTAL = data.readline().strip()
    for line in data:
        TEST_CASES.append(line.split())

SOLUTION = [0] * int(TOTAL)

with open('words.txt') as words:
    for word in words:
        for test in range(int(TOTAL)):
            temp = list(word.strip())
            test_case = TEST_CASES[test]
            if (len(temp) == int(test_case[0]) and
                    all(elem in test_case for elem in temp)):
                for key in test_case[1:]:
                    try:
                        temp.remove(key)
                    except ValueError:
                        continue
            if not temp:
                SOLUTION[test] += 1

print ' '.join(map(str, SOLUTION))

# $ python jicardona.py
# 1 8 4 17 8 6 1 3 9 37 9 8 6 7

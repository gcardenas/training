/*
$ eslint actiradob.js
$
*/

function count(line, words) {
  let amount = 0;
  const numLet = parseInt(line.shift(), 10);
  const arrayWords = [];
  words.map((word) => {
    const studioWord = word.split('');
    line.map((letter) => {
      const index = studioWord.indexOf(letter);
      const indexNull = -1;
      if (index !== indexNull) {
        studioWord.splice(index, 1);
      }
      if (studioWord.length === 0 && word.length === numLet) {
        if (!arrayWords.includes(word)) {
          arrayWords.push(word);
          amount += 1;
        }
      }
      return 'amount';
    });
    return 'amount';
  });
  return amount;
}

function main(misA, filB, lines) {
  const words = filB.split(/\r\n|\r|\n/g);
  let amount = lines.map((line) => count(line, words));
  amount = amount.join(' ');
  process.stdout.write(amount);
}

const fileS = require('fs');

function four(miss, file) {
  let lines = file.split(/\r\n|\r|\n/g);
  lines.shift();
  lines = lines.map((value) => value.split(' '));
  fileS.readFile('words.txt', 'utf8', (misA, filB) => main(misA, filB, lines));
}

function fileLoad() {
  fileS.readFile('DATA.lst', 'utf8', (miss, file) => four(miss, file));
}

fileLoad();

/*
$ node actiradob.js
1 8 4 17 8 6 1 3 9 37 9 8 6 7
*/

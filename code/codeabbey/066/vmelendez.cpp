/*
$ cppcheck --enable=all --check-config vmelendez.cpp
Checking vmelendez.cpp ...
$ g++ vmelendez.cpp -o vmelendez
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAX_NAME_SZ 256
#define ALPHABET_LEN 26
#define NUM_KEYS 25

void caesar_crack(char *ciphertext) {
  char alphabet[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G',
  'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
  'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
  char dict[ALPHABET_LEN];
  char deciphered_sentences[MAX_NAME_SZ][MAX_NAME_SZ];
  char deciphered[strlen(ciphertext)];
  char *p;

  int k = 0, min_average = 0;
  for (int i = 0; i < NUM_KEYS; i++) {
    for (int i = 0; i < ALPHABET_LEN; i++) {
      dict[(i + k) % ALPHABET_LEN] = alphabet[i];
    }
    k++;

    for (int i = 0; i < strlen(ciphertext); i++) {
      p = strchr(alphabet, ciphertext[i]);
      if (ciphertext[i] != ' ' && p-alphabet >= 0) {
        deciphered[i] = dict[p-alphabet];
      } else {
        deciphered[i] = ciphertext[i];
      }
    }

    strcpy(deciphered_sentences[i], deciphered);

    float frequency[ALPHABET_LEN];
    float count[ALPHABET_LEN] = {0};
    int c = 0, x;
    for (int i = 0; i < ALPHABET_LEN; i++) {
      while (deciphered[c] != '\0') {
        x = deciphered[c] - 'A';
        count[x]++;
        c++;
      }
    }

    for (int i = 0; i < ALPHABET_LEN; i++) {
      frequency[i] = count[i] / strlen(ciphertext) * 100;
    }

    float ideal_letters[] = {8.1, 1.5, 2.8, 4.3, 13.0, 2.2,
    2.0, 6.1, 7.0, 0.15, 0.77, 7.0, 2.4, 6.8, 7.5, 1.9,
    0.095, 6.0, 6.3, 9.1, 2.8, 0.98, 2.4, 0.15, 2.0, 0.074};
    float diff[ALPHABET_LEN];
    for (int i = 0; i < ALPHABET_LEN; i++) {
      diff[i] = frequency[i] - ideal_letters[i];
      diff[i] *= diff[i];
    }

    float averages[ALPHABET_LEN];
    float sum = 0, average;
    for (int i = 0; i < ALPHABET_LEN; i++) {
      sum += diff[i];
    }

    average = sum / ALPHABET_LEN;
    averages[i] = average;

    for (int i = 0; i < NUM_KEYS; i++) {
      if (averages[i] < averages[min_average])
        min_average = i;
    }
  }

  char *token;
  token = strtok(deciphered_sentences[min_average], " ");
  int n = 0;
  while( n != 3 ) {
    printf("%s ", token);
    token = strtok(NULL, " ");
    n++;
  }
  printf("%d ", min_average);
}

int main(int argc, char const *argv[]) {
  FILE *fp;
  fp = fopen("DATA.lst", "r");
  char ciphertext[MAX_NAME_SZ];
  char no[2];
  fgets(no, 2, fp);
  int n = atoi(no);
  fgets(no, 2, fp);
  for (int i = 0; i < n; i++) {
    fgets(ciphertext, MAX_NAME_SZ, fp);
    ciphertext[strlen(ciphertext) - 1] = '\0';
    caesar_crack(ciphertext);
  }
  fclose(fp);
}
/*
$ ./vmelendez
POETRY IS WHAT 5 CARTHAGE MUST BE 22 NO SOONER SPOKEN 21
IT IS BLACK 6 THAT ALL MEN 18
*/

/*
$ rustup run nightly cargo clippy
Compiling skhorn v0.1.0 (file:///../skhorn)
Finished dev [unoptimized + debuginfo] target(s) in 0.24 secs
$ rustc skhorn.rs
$
*/
#![allow(unused)]
#![allow(unknown_lints)]
#![allow(redundant_field_names)]
#![allow(needless_range_loop)]
#![allow(explicit_counter_loop)]


//Read file 
use std::collections::HashMap;
use std::io::{BufReader,BufRead};
use std::fs::File;

fn main() {
  
  let file = File::open("DATA.lst").unwrap();

  let mut output_vec = Vec::new(); 
  let mut frequency_letter_hash = HashMap::new();
  let mut res_arr = Vec::new();
  let mut lines_counter = 0;
  let mut char_index: i32 = 0;

  for line in BufReader::new(file).lines() {
    
    let current_line: &str = &line.unwrap();
    if current_line.len() > 1 {
      // Get frequency by letter
      frequency_letter_hash = count_letters(current_line);
      
      // Get maximum value
      // In order to avoid brute forcing, we retrieve
      // maximum values on the frequency analysis performed
      let mut max_val_arr = Vec::new();
      for (key, value) in frequency_letter_hash {
        if value > 1 {
          max_val_arr.push(key);
          //println!("Key[Letter[{}]] Value:{}", key, value);         
        }
      }
      
      let mut res_arr_clone = Vec::new();
      let mut bool_res = false;
      // First: We need to get the index of the most frequent char
      for letter in max_val_arr {
        char_index = get_alpha_index(&letter.to_string());
        res_arr = pseudo_cracker(char_index, current_line);

        // Cloning result array, so it can be used later
        res_arr_clone = res_arr.clone();
        // If the purpose is to crack the encoded input
        // while decoding it, should have an english equivalence
        bool_res = search_common_words(&res_arr);
        //println!("{}", bool_res);
        if bool_res {
          break;
        }
      }
      if bool_res {
        // We found a match! Order it 
        //println!("{} {:?} {}", bool_res, res_arr_clone, char_index);
        let mut count = 0;
        for i in 0 .. res_arr_clone.len() {
          //println!(" ========= {} ", res_arr_clone[i]);
          if i < 3 {
            let mut item = res_arr_clone[i].clone();
            output_vec.push(item);
          }
          count += 1;
        }
        output_vec.push(char_index.to_string());
      } else {
        // Frequency analysis did not throw any good result
        // Brute force it
        // Lets iterate over all the alphabet index
        for i in 0 .. 26 {
          // Lets crack it and see it what's the result
          res_arr = pseudo_cracker(i, current_line);
          // Clone it -- rust necesity.
          res_arr_clone = res_arr.clone();
          // Seek any match on common words
          bool_res = search_common_words(&res_arr);
          //println!("BRUTE FORCING VALUE {}", bool_res);
          if bool_res {
            //lines_counter += 1;
            //println!("{} {:?} {}", bool_res, res_arr_clone, i);
            let mut count = 0;
            // Take the K, and the first three elements and 
            // store them
            for i in 0 .. res_arr_clone.len() {
              //println!(" ========= {} ", res_arr_clone[i]);
              if i < 3 {
                let mut item = res_arr_clone[i].clone();
                output_vec.push(item);
              }
              count += 1;
            }
          output_vec.push(i.to_string());
          break;
          }
        }
      }
    }
    lines_counter += 1;
  }
  //println!("{:?}", output_vec);
  // Print the result
  let mut result: String = Default::default();
    for item in &output_vec {
      result.push(' ');
      result.push_str(item);
    }
    println!("{}", result);
}


// Ineffecient frequency analysis counter
// It takes the input and counts each concurrency storing values
// In a key,value form, HashMap
fn count_letters(encoded: &str) -> std::collections::HashMap<char, i32> {  
 let mut alpha = HashMap::new();
 let alphabet = (b'A' .. b'Z' + 1)        // Start as u8
        .map(|c| c as char)               // Convert all to chars
        .filter(|c| c.is_alphabetic())    // Filter only alphabetic chars
        .collect::<Vec<_>>();
  
  for char in alphabet {
    let counter = alpha.entry(char).or_insert(0);
    *counter = 0;
  }
  
  for ch in encoded.chars() {
    if ch != ' ' {
      let counter = alpha.entry(ch).or_insert(0);
      *counter += 1;
    }
  }
  //println!("Encoded word [{}]\nFrequency Analysis {:?}", encoded, alpha);
 alpha 
}

// Get the index from specific character in alphabet
fn get_alpha_index(alpha_char: &str) -> i32 {
  let mut char_index: i32 = Default::default();
  let alphabet = (b'A' .. b'Z' + 1)        // Start as u8
        .map(|c| c as char)               // Convert all to chars
        .filter(|c| c.is_alphabetic())    // Filter only alphabetic chars
        .collect::<Vec<_>>();
   
  for index in 0 .. alphabet.len() {
    if alpha_char == alphabet[index].to_string() {
      char_index = index as i32;
      //println!("Char[{}] - Index[{}]", alphabet[index], index);
      break;
    }
  }
  char_index
}

// Generating Key based on frequency analysis
fn pseudo_cracker(guess_key: i32, encoded_word: &str) -> std::vec::Vec<std::string::String> {
  use std::char;
  
  // Generating alphabet based on key index
  let cipher_key: u32 = guess_key as u32;
  let start: u32 = 10 + cipher_key;

  let alpha = (start..36)
          .map(|i| char::from_digit(i, 36)
          .unwrap())
          .collect::<Vec<_>>();
  
  let mut alpha = alpha
                  .iter()
                  .map(|c| c.to_uppercase()
                  .next()
                  .unwrap())
                  .collect::<Vec<_>>();

  let bet = (10..start)
          .map(|i| char::from_digit(i, 36)
          .unwrap())
          .collect::<Vec<_>>();

  let mut bet = bet
          .iter()
          .map(|c| c.to_uppercase()
          .next().unwrap())
          .collect::<Vec<_>>();

  alpha.append(&mut bet);
  //println!("{:?}", alpha);
  

  //println!("TRYING TO CRACK THAT! == {}", encoded_word);
  let alphabet = (b'A' .. b'Z' + 1) 
            .map(|c| c as char)
            .filter(|c| c.is_alphabetic())
            .collect::<Vec<_>>();
  //println!("{:?}", alphabet);
  

  let mut decoded_word: String = Default::default(); 
  let mut decoded_array_words = Vec::new();
  for item in encoded_word.chars() {
    if item == ' ' {
      decoded_array_words.push(decoded_word);
      //println!(" ============ ============  {}", decoded_word);  
      decoded_word = Default::default(); 
    } else {
      let enc_index = alpha.iter().position(|&r| r.to_string() == item.to_string()).unwrap();
      //println!("ENCODED[{}] - DECODED[{}]", item, alphabet[enc_index]);
      decoded_word += &alphabet[enc_index].to_string();
    } 
  }
  //println!(" ============ ============  {}", decoded_word);  
  decoded_array_words.push(decoded_word);
  //println!("DECODEDDDDDDD {:?}", decoded_array_words);
  
  // Return decoded array
  decoded_array_words
  
}

fn search_common_words(arr: &[std::string::String]) -> bool {

  let mut bool_result = false;
  let common_words = vec!["A", "ABOUT", "ALL", "ALSO", "AND", "AS", 
                          "AT", "BE", "BECAUSE", "BUT", "BY", "CAN",
                          "COME", "COULD", "DAY", "DO", "EVEN", "FIND",
                          "FIRST", "FOR", "FROM", "GET", "GIVE", "GO", 
                          "HAVE", "HE", "HER", "HERE", "HIM", "HIS", 
                          "HOW", "IF", "IN", "INTO", "IT", "ITS", 
                          "JUST", "KNOW", "LIKE", "LOOK", "MAKE", "MAN", 
                          "MANY", "ME", "MORE", "MY", "NEW", "NO", "NOT",
                          "NOW", "OF", "ON", "ONE", "ONLY", "OTHER", 
                          "OUR", "OUT", "PEOPLE", "SAY", "SEE", "SHE", "SO", 
                          "SOME", "TAKE", "TELL", "THAN", "THAT",  "THE", 
                          "THEIR", "THEM", "THEN", "THERE", "THESE", "THEY", 
                          "THING", "THINK", "THIS", "THOSE", "TIME", "TO", 
                          "TWO", "UP", "USE", "VERY", "WANT", "WAY", "WE", 
                          "WELL", "WHAT", "WHEN", "WHICH", "WHO", "WILL", 
                          "WITH",  "WOULD", "YEAR", "YOU","YOUR"];
  
  //println!("{:?}", arr);
  for item in arr.iter() {
    //println!("{}", item);
    let result = common_words.iter().find(|&&x| x == item);
    //println!("RESULTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT {:?}", result);
    if result != None {
      bool_result = true;
      break;
    }
  }
  bool_result
}

/*
$ ./skhorn 
POETRY IS WHAT 5 CARTHAGE MUST BE 22 NO SOONER SPOKEN 21 IT IS BLACK 6 THAT ALL MEN 18
*/

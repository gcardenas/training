#!/usr/bin/python3
""" Codeabbey 64: Path-Finder
*** Sample run with DATA.lst
$ python scudriz.py
4D4L2D4R4D14L8U2L2D10L2D2L2U2L4U4L /
2R2U6R2U2R4U4R2D6R2U2L2U2L4U4R6U2L2D10L2D2L2U2L4U4L /
2U4L6U12L2U2L4U4R6U2L2D10L2D2L2U2L4U4L
*** Linter output
$ pylint scudriz.py
No config file found, using default configuration
************* Module scudriz
C:133, 0: Final newline missing (missing-final-newline)
W: 91, 4: Using the global statement (global-statement)
W: 92, 4: Using the global statement (global-statement)
W: 93, 4: Using the global statement (global-statement)

------------------------------------------------------------------
Your code has been rated at 9.45/10 (previous run: 9.45/10, +0.00)
"""

# Global variables
HEIGHT = 0
WIDTH = 0
MATRIX = []
SOLUTION = []

def check_coordinate(point):
    '''
    Function that defines wether a coordinate is valid or not
    '''
    if point[0] < 0 or point[0] > (HEIGHT - 1):
        return False

    if point[1] < 0 or point[1] > (WIDTH - 1):
        return False

    if MATRIX[point[0]][point[1]] == 0:
        return False

    return True


def path_finder(point, prev=0):
    '''
    Function that determines the path from a point to coordinate (0, 0)
    '''
    #checks for solution
    if point == [0, 0]:
        return True

    # posibles movements up, right, down and left
    to_up = [point[0] - 1, point[1]]
    right = [point[0], point[1] + 1]
    down = [point[0] + 1, point[1]]
    left = [point[0], point[1] - 1]

    if check_coordinate(to_up) and prev != 'D' and path_finder(to_up, 'U'):
        SOLUTION.append('U')
        return True

    if check_coordinate(down) and prev != 'U' and path_finder(down, 'D'):
        SOLUTION.append('D')
        return True

    if check_coordinate(left) and prev != 'R' and path_finder(left, 'L'):
        SOLUTION.append('L')
        return True

    if check_coordinate(right) and prev != 'L' and path_finder(right, 'R'):
        SOLUTION.append('R')
        return True

    return False


def check_format(raw):
    '''
    Function that changes the output of path_finder into a more human
    readable text. For instance: LLLDDRRUUUU -> 3L2D2R4U
    '''

    prev = raw[0]
    counter = 1
    text = ''
    for i in raw[1:]:
        if i == prev:
            counter += 1
        else:
            text += str(counter) + prev
            prev = i
            counter = 1
    return text + str(counter) + prev

def main():
    '''
    Function that reads the test case from codeabbey
    '''
    global HEIGHT
    global WIDTH
    global SOLUTION

    archivo = open('DATA.lst', 'r')

    line1 = archivo.readline()
    [widtho, heighto] = line1.split()
    HEIGHT = int(heighto)
    WIDTH = int(widtho)

    for line in archivo:
        aux = []

        if line[-1] == '\n':
            line = line[:-1]

        for i in line:
            aux.append(int(i))

        MATRIX.append(aux)

    # from a
    SOLUTION = []
    path_finder([0, WIDTH - 1])
    SOLUTION.reverse()
    point_a = check_format(SOLUTION)

    # from b
    SOLUTION = []
    path_finder([HEIGHT-1, 0])
    SOLUTION.reverse()
    point_b = check_format(SOLUTION)

    # from c
    SOLUTION = []
    path_finder([HEIGHT-1, WIDTH-1])
    SOLUTION.reverse()
    point_c = check_format(SOLUTION)

    print(point_a+' '+point_b+' '+point_c)

main()

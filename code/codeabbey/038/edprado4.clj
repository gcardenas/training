(ns quadraticequation.core
  (:gen-class))

(defn aproximar [numero]
  (def umbralaprox 0.5)
  (def entero (int (* 1 numero)))
  (def decimal (- numero entero))
  (if (>= decimal umbralaprox) 
    (def entero (inc entero)))
  (if (<= decimal  (* -1 umbralaprox))
    (def entero (- entero 1)))
  (* 1 entero))

(defn complexnum [n]
  (str n "i"))

(defn absvalue [n]
  (if (< n 0)(* n -1)(* n 1)))

(defn -main []
  (def data [[2 -28 170] [4 -48 80] [1 1 -6] [4 76 360] [5 10 250] [4 24 40] [6 -120 1200] [2 -12 50] [6 -96 984] [4 -72 424] [3 60 447] [2 -12 146] [2 8 80] [4 28 -72] [9 162 720]])
  (dotimes [i (count data)]
    (let [datos (data i) A (datos 0) B (datos 1) C (datos 2) root (- (* B B) (* 4 A C))]
      (if (< root 0) 
        (do ( def q1 (str (aproximar(/ (* -1 B) (* 2 A))) "+" (complexnum ( aproximar ( / (Math/sqrt (absvalue root )) (* 2 A))))))
          ( def q2 (str (aproximar(/ (* -1 B) (* 2 A))) "-" (complexnum ( aproximar ( / (Math/sqrt (absvalue root )) (* 2 A)))))))
        (do ( def q1 (aproximar (+ (/ (* -1 B) (* 2 A)) (/ (Math/sqrt root) (* 2 A)))))
          ( def q2 (aproximar (- (/ (* -1 B) (* 2 A)) (/ (Math/sqrt root) (* 2 A)))))))
      
      (if (== (inc i) (count data)) (print (str q1 " " q2 " "))(print (str q1 " " q2 "; "))))))


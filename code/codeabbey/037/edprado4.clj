(ns mortagecalculator.core
  (:gen-class))

(defn aproximar [numero]
  (def umbralaprox 0.5)
  (def entero (int (* 1 numero)))
  (def decimal (- numero entero))
  (if (>= decimal umbralaprox) 
    (def entero (inc entero)))
  (if (<= decimal  (* -1 umbralaprox))
    (def entero (- entero 1)))
  (* 1 entero))

(defn calchipoteca [p i m c]
  (def newp p)
  (def newi i)
  (println "Mes     P           P * (I/12)%       -C         nuevo P")
  (dotimes [j  (- m 1)]
    (def newi (aproximar (* i newp)))
    (if (not= j (- m 1)) (def deuda (+ newp  newi (* -1 c))))
    (println (inc j)"  "(aproximar newp)"        " newi "         -"(aproximar c) "      "(aproximar deuda))   
    (def newp deuda))
 (println  m "  "(aproximar newp)"        " (aproximar (* i newp)) "         -"(aproximar deuda )"      "0))

(defn -main [] 
  (def prestamo 1600000)
  (def interes (float (/ 17 100 12)))
  (def meses 72)
  (def a (/ (- 1 (Math/pow (+ 1 interes) (* -1 meses))) interes))
  (def cuotas (/ prestamo a))
  (calchipoteca prestamo interes meses cuotas)
  (println (aproximar cuotas)))

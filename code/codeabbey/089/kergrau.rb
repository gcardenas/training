# $rubocop kergrau.rb
# Inspecting 1 file
# .
#
# 1 file inspected, no offenses detected
require 'facets'

answer = ''
flag = false
file = File.new('DATA89.lst', 'r')

def octa
  [32.70, 34.65, 36.71, 38.89, 41.20, 43.65,
   46.25, 49.00, 51.91, 55.00, 58.27, 61.74]
end

def add_note(octave)
  octave << octave[11] / 2
  octave << octave[1] * 2
end

while (line = file.gets)
  unless flag
    flag = true
    next
  end

  line.split(' ').each do |frec|
    octave = octa
    scale = 1
    note = []

    while frec.to_i > octave.max
      octave = octave.ewise * 2
      scale += 1
    end

    add_note octave

    octave.each { |num| note << (num - frec.to_i).abs }

    case note.index(note.min)
    when 0 then answer << "C#{scale} "
    when 1 then answer << "C##{scale} "
    when 2 then answer << "D#{scale} "
    when 3 then answer << "D##{scale} "
    when 4 then answer << "E#{scale} "
    when 5 then answer << "F#{scale} "
    when 6 then answer << "F##{scale} "
    when 7 then answer << "G#{scale} "
    when 8 then answer << "G##{scale} "
    when 9 then answer << "A#{scale} "
    when 10 then answer << "A##{scale} "
    when 11 then answer << "B#{scale} "
    when 12 then answer << "B#{scale - 1} "
    when 13 then answer << "C#{scale + 1} "
    end
  end
end
puts answer
# ruby kergrau.rb
# F#3 A#2 G#3 A1 G#1 F2 A#5 A#3 B1 C2 F#4 G5

data = [x.split() for x in """2190 2250
93 435
880 2560
782 6
22 7788
9 5
3100 2500
17 760
1053 1131
81 969
5211 4
2 14
55 86
4503 3713
2 5
660 2190
5600 5280
8 10""".splitlines()]

# greatest common divisor
def gcd(a, b):
    while b != 0:
        a, b = b, a % b
    return a

# least common multiple
def lcm(a, b):
    return int(a * b / gcd(a, b))

for array in data:
    print("({} {})".format(gcd(int(array[0]), int(array[1])), lcm(
        int(array[0]), int(array[1]))), end=" ")

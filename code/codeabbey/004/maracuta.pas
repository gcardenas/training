program maracutamintwo; {Nombre del programa}

uses
  SysUtils;

var
  Num2, control, x, y, res: longint; {Declaro variables como enteras}

  vector, vectorcomp: array [1..100] of longint;

begin

  {Inicializo variables para prevenir error de compilacion}
  Num2 := 0;
  res := 0;
  control := 0;

  {Solicito variable de control}
  writeLn('Introduzca la variable de control: ');
  readLn(control);

  writeLn('Escriba los pares de numeros separados por espacio:');
  for x := 1 to control do
  begin
    y := 2;
    repeat

      Read(Num2);
      vectorcomp[y] := Num2;
      y := y - 1;

    until y = 0;

    if (vectorcomp[1] > vectorcomp[2]) then
    begin
         vector[x]:=vectorcomp[2];
    end
    else vector[x]:=vectorcomp[1];


  end;


  for x := 1 to control do
  begin
    Write(vector[x]);
    Write(' ');
  end;

  readLn();
  readLn();

end.

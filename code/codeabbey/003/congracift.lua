--[[
  $ luacheck congracift.lua
Checking congracift.lua                           OK

Total: 0 warnings / 0 errors in 1 file

--]]

local function sumLoop(file)
  local sum = 0
  local dataTable = {}
  local flag = true
  for line in io.lines(file) do
  --ignore firts line in the file
    if (flag == false) then
      for i in string.gmatch(line, "%S+") do
        sum = sum + tonumber(i)
      end
  end
  --add to table
  if (flag == false) then
    table.insert(dataTable,sum)
  end
  flag = false
    sum = 0
  end
  return dataTable
end

local dataTable = sumLoop("DATA.lst")
io.write(table.concat(dataTable," "))

--[[
  $ /usr/bin/env lua congracift.lua
  $ 811288 233523 803572 1035449 1261327 1409775 995524 1279923 1128418 1483115
  1016280 449446 833641
--]]

<?php
/*
PHP version 7.2.11.0

Linting with "SublimeLinter-PHP"

phpcs ciberstein.php

Compiling and linking using the "Command Windows prompt"

> C:\\..\php \\..\ciberstein.php
./output
*/
if(file_exists('./DATA.lst')) {
  $file = fopen('./DATA.lst',"r");
  $line = fgets($file);

  for($i=0; $i < $line; $i++) {
    $input = explode(" ", fgets($file));

    $s = $input[0];
    $t = $input[1];
    $p = $input[2] / 100;

    $end = "0";

    while($s < $t) {
      $s = ($s * $p) + $s;
      $end++;
    }
  echo $end." ";
  }
}
else
  echo "Error 'DATA.lst' not found";
/*
./ciberstein.php
9 31 8 8 10 26 7 43 8 66 7 100 37 48 6 17 10 8
*/
?>

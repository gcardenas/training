/*
$ dmd simongomez95.d
*/

import std.stdio;
import std.file;
import std.string;
import std.conv;

void main(string[] args) {
  File file = File("DATA.lst", "r");
  string data = file.readln();
  string[] numbers = data.split(" ");
  int a = to!int(numbers[0]);
  int b = to!int(numbers[1]);
  int sum = sum(a, b);
  file.close();
  writeln(sum);
}

int sum(int a, int b) {
  int sum = a+b;
  return sum;
}

/*
$ ./simongomez95
18772
*/

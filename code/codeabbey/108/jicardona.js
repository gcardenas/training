#!/usr/bin/env jsc
/*
$ eslint jicardona.js
*/

/* global arguments, print */

/* eslint-disable fp/no-arguments */
const [ input ] = arguments;
/* eslint-enable fp/no-arguments */

const testCases = input.split(/\n/);
const [ total ] = testCases;

function countGems(index, answer) {
  if (index < 0) {
    return answer.join(' ');
  }
  const testCase = testCases[total - index].split(/\s/);
  const result = testCase[0] * (testCase[1] - 1);
  return countGems(index - 1, answer.concat(result));
}

/* eslint-disable fp/no-unused-expression*/
print(countGems(total - 1, []));
/* eslint-enable fp/no-unused-expression*/

/*
$ jsc jicardona.js -- "`cat DATA.lst`"
20 51 27 102 32 57 15 11 17
*/

#!/usr/bin/env python3
"""
Problem #14 Modular calculator
"""
class ModularCalculator:
    total = 0
    def __init__(self):

        while True:
            line = input()

            if line:
                test_case = line.split()
                self.operations(*test_case)
            else:
                break

    def operations(self, *args):
        if len(args) == 1:
            self.total = int(args[0])
        else:
            if args[0] is "+":
                self.total += int(args[1])

            elif args[0] is "*":
                self.total *= int(args[1])

            elif args[0] is "%":
                self.total = self.total % int(args[1])

    print(self.total)


ModularCalculator()

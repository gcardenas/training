
print_endline "Ingrese la cantidad de Casos a Verificar:";;

let sumr su =
  (if su >= 0.00
  then su +. 1.0
  else su -. 1.0)
  |> truncate


let () =
  Scanf.scanf "%d " (fun n ->
                      for i = 0 to n - 1 do
                        (Scanf.scanf "%f " (fun num  -> num *. float_of_int 6  |> sumr))
                            |>Printf.printf "%d "
                      done)

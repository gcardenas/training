#!/usr/bin/env jsc
/*
$ eslint jicardona.js
*/

/* global arguments, print */

/* eslint-disable fp/no-arguments */
const [ input ] = arguments;
/* eslint-enable fp/no-arguments */

const set = input.split(/\s/);

const aValue = 445;
const cValue = 700001;
const period = 2097152;

const mult = 4;

function randomGenerator(counter, seed, random) {
  if (counter >= set[0] * mult) {
    return random;
  }
  const xNext = ((aValue * seed) + cValue) % period;
  const result = (xNext % set[0]) + 1;
  return randomGenerator(counter + 1, xNext, random.concat(result));
}

function connectEdges(vertex, secuence, graph) {
  if (typeof graph[vertex] === 'undefined') {
    return Object.assign([ ...graph ], { [vertex]: [ secuence ] });
  } else if (graph[vertex].length ===
    (graph[vertex].filter((current) => current[0] !== secuence[0])).length) {
    return Object.assign([ ...graph ],
      { [vertex]: graph[vertex].concat([ secuence ]) });
  }
  return graph;
}

function graphGenerator(vertex, step, secuence, graph) {
  if (secuence.length <= 0) {
    return graph;
  }
  const newVertex = step % 2 === 0 ? vertex + 1 : vertex;
  if (newVertex === secuence[0]) {
    return graphGenerator(newVertex, step + 1, secuence.slice(2), graph);
  }
  const newGraph = connectEdges(secuence[0], [ newVertex, secuence[1] ], graph);
  return graphGenerator(newVertex, step + 1, secuence.slice(2),
    connectEdges(newVertex, [ secuence[0], secuence[1] ], newGraph));
}

function sumWeights(current, index, arr, edge = 0, sum = 0) {
  if (edge >= current.length) {
    return sum;
  }
  return sumWeights(current, index, arr, edge + 1, sum + current[edge][1]);
}

const answer = graphGenerator(0, 0, randomGenerator(0, set[1], []), []);

/* eslint-disable fp/no-unused-expression*/
print((answer.slice(1).map(sumWeights)).join(' '));
/* eslint-enable fp/no-unused-expression*/

/*
$ jsc jicardona.js -- "`cat DATA.lst`"
12 35 8 19 15 33 15 5 29 8 21
*/

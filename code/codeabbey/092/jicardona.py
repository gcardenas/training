#!/usr/bin/env python2.7
"""
$ pylint jicardona.py
No config file found, using default configuration

------------------------------------
Your code has been rated at 10.00/10
"""

with open('DATA.lst') as data:
    TOTAL = data.readline().strip()
    COMMANDS = data.readline().split()

BINARY_HEAP = []

for command in COMMANDS:
    if command == '0':
        try:
            BINARY_HEAP[0] = BINARY_HEAP.pop()
        except IndexError:
            pass
        index = pointer = 0
        child = 2 * index
        while index == pointer:
            if child + 1 < len(BINARY_HEAP):
                if BINARY_HEAP[child + 1] < BINARY_HEAP[pointer]:
                    pointer = child + 1
            if child + 2 < len(BINARY_HEAP):
                if BINARY_HEAP[child + 2] < BINARY_HEAP[pointer]:
                    pointer = child + 2
            if pointer != index:
                temp = BINARY_HEAP[pointer]
                BINARY_HEAP[pointer] = BINARY_HEAP[index]
                BINARY_HEAP[index] = temp
                index = pointer
                child = 2 * index
            else:
                break
    else:
        BINARY_HEAP.append(int(command))
        index = len(BINARY_HEAP) - 1
        father = (index - 1) / 2
        while BINARY_HEAP[index] < BINARY_HEAP[father] and index > 0:
            temp = BINARY_HEAP[father]
            BINARY_HEAP[father] = BINARY_HEAP[index]
            BINARY_HEAP[index] = temp
            index = father
            father = (index - 1) / 2

print ' '.join(str(result) for result in BINARY_HEAP)

# $ python jicardona
# 5 10 6 14 13 11 7 17 15 16 19 20 21 31 27 24 18 25 28 30 22 32 23 26 29

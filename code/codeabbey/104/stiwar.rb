data = []
out = []
def operations(x1,y1,x2,y2,x3,y3)
  p1 = (y2-y1)**2
  p2 = (x2-x1)**2
  p3 = (y3-y1)**2
  p4 = (x3-x1)**2
  p5 = (y3-y2)**2
  p6 = (x3-x2)**2
  a = Math.sqrt(p1 + p2)
  b = Math.sqrt(p3 + p4)
  c = Math.sqrt(p5 + p6)
  s = ((a + b + c )/2)

  area = Math.sqrt(s*(s-a)*(s-b)*(s-c)).round(7)
  return area
end

puts 'ingrese el numero de pruebas'
p = gets.chomp.to_i

c=0
(1..p).each do
  da = []
  puts "(#{c+1}) ingrese las tres coordenadas del triangulo separadas por un espacio:"
  da = gets.chomp.split(" ")
  k = 0
  da.each do
    da[k] = da[k].to_f
    k+=1
  end
  data[c] = da
  c += 1
end

c = 0
(1..p).each do
  out[c] = operations(data[c][0],data[c][1],data[c][2],data[c][3],data[c][4],data[c][5])
    c += 1
end
puts out.map(&:inspect).join(" ")

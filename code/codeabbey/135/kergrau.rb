# ruby-lint kergrau.rb

answer = ''
union = ''
sequence = ''
File.open('DATA.lst', 'r') do |file|
  while line = file.gets
    line.strip.split('').each do |letter|

      case letter
        when ' '
          sequence = '11'
        when 't'
          sequence = '1001'
        when 'n'
          sequence = '10000'
        when 's'
          sequence = '0101'
        when 'r'
          sequence = '01000'
        when 'd'
          sequence = '00101'
        when '!'
          sequence = '001000'
        when 'c'
          sequence = '000101'
        when 'm'
          sequence = '000011'
        when 'g'
          sequence = '0000100'
        when 'b'
          sequence = '0000010'
        when 'v'
          sequence = '00000001'
        when 'k'
          sequence = '0000000001'
        when 'q'
          sequence = '000000000001'
        when 'e'
          sequence = '101'
        when 'o'
          sequence = '10001'
        when 'a'
          sequence = '011'
        when 'i'
          sequence = '01001'
        when 'h'
          sequence = '0011'
        when 'l'
          sequence = '001001'
        when 'u'
          sequence = '00011'
        when 'f'
          sequence = '000100'
        when 'p'
          sequence = '0000101'
        when 'w'
          sequence = '0000011'
        when 'y'
          sequence = '0000001'
        when 'j'
          sequence = '000000001'
        when 'x'
          sequence = '00000000001'
        when 'z'
         sequence = '000000000000'
      end
      union << sequence
    end

    union.chars.each_slice(8) do |e|
      substring = e.join('')
      if substring.length < 8
        #fill with 0 to rigth until have 8 chars
        substring = substring.ljust(8, '0')
      end
      answer = substring.to_i(2).to_s(16)
      if answer.length < 2
        #fill with 0 to left until have 2 chars
        answer = answer.rjust(2, '0')
      end
      puts answer.upcase
    end
  end
end
# ruby kergrau.rb
# 93 B8 51 82 25 22 E1 23 12 E4 1D CD AE 24 50 09 25 2D 86 69 5C 80 91 01 A8
# 84 50 BC C7 0A D5 72 09 60 D7 88 9A 43 0E 95 2E 6E 42 28 54 55 B0 97 29 8C
# 34 C3 93 B9 04 D0 89 52 5C 8D 17 7A 12 10 E7 4C 2A CA 43 60 89 BC C7 93 A1
# EE 05 D3 0C 37 00 38 AD 6A F1 13 93 B8 2B 0A 22 67 88 99 12 84 B2 70 40 72
# 00 23 40 1C 81 22 8E 50 E0 A1 62 89 4C 02 63 5C 15 03 18 17 21 6B 50

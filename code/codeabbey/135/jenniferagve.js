/*  Feature: Solving the challenge #135 Variable Length Code
  With JavaScript - Node.js
  From http://www.codeabbey.com/index/task_view/variable-length-code

    Linting:   eslint jenniferagve.py
    --------------------------------------------------------------------
    141:1  error  Unused expression  fp/no-unused-expression
    ✖ 1 problem (1 error, 0 warnings)
*/
/* eslint array-element-newline: ["error", "consistent"]*/
const diccionario = [
  ' ',
  't',
  'n',
  's',
  'r',
  'd',
  '!',
  'c',
  'm',
  'g',
  'b',
  'v',
  'k',
  'q',
  'e',
  'o',
  'a',
  'i',
  'h',
  'l',
  'u',
  'f',
  'p',
  'w',
  'y',
  'j',
  'x',
  'z',
];
const binValues = [
  '11',
  '1001',
  '10000',
  '0101',
  '01000',
  '00101',
  '001000',
  '000101',
  '000011',
  '0000100',
  '0000010',
  '00000001',
  '0000000001',
  '000000000001',
  '101',
  '10001',
  '011',
  '01001',
  '0011',
  '001001',
  '00011',
  '000100',
  '0000101',
  '0000011',
  '0000001',
  '000000001',
  '00000000001',
  '000000000000',
];
/* eslint no-magic-numbers: ["error", { "ignore": [16, 9, 8, 0, 1] }]*/
function complete(binUnion, times) {
  /* Calculate the number of 0's missing on the array and complete them */
  const newCorrespBin = binUnion + 0;
  const newTimes = times - 1;
  if (newTimes === 0) {
    return newCorrespBin;
  }
  return complete(newCorrespBin, newTimes);
}

function convertHex(element) {
  /* Convert the binary value into hexadecimal values */
  const binNumber = (parseInt(element, 2).toString(16)).toUpperCase();
  if ((binNumber <= 9) || (binNumber === 'A') || (binNumber === 'B')) {
    const beforeNum = '0';
    const totalHex = beforeNum.concat(binNumber);
    return [ totalHex ];
  } else if ((binNumber === 'C') || (binNumber === 'D')) {
    const beforeNum = '0';
    const totalHex = beforeNum.concat(binNumber);
    return [ totalHex ];
  } else if ((binNumber === 'E') || (binNumber === 'F')) {
    const beforeNum = '0';
    const totalHex = beforeNum.concat(binNumber);
    return [ totalHex ];
  }
  return [ binNumber ];
}

function segmentation(timesCount, finalTimes, newCorrespBin, totalArray) {
  /* segment the hole vale of binary in octets to be process */
  const finalArray = totalArray.concat(
    [ newCorrespBin.substr((timesCount * 8), 8) ]);
  if (timesCount === (finalTimes)) {
    return finalArray;
  }
  return segmentation(timesCount + 1, finalTimes, newCorrespBin, finalArray);
}

/* eslint no-sync: ["error", { allowAtRootLevel: true }]*/
const filesre = require('fs');
const contents = filesre.readFileSync('DATA.lst', 'utf8', '\n');
function fileprocess() {
  /* Read the line of the file and save the binary value of each chatacter*/
  const info = contents.toString().split('');
  const indexValue = info.map((element) => diccionario.indexOf(element));
  const correspBin = indexValue.map((element) => (binValues[element]));
  const binUnion = correspBin.join('');
  return binUnion;
}

function main() {
  /* Main where each function is called to calculate the final hexadecimal
  values of the variable length */
  const binUnion = fileprocess();
  const times = ((parseInt((binUnion.length / 8),
    10) * 8) + 8) - binUnion.length;
  const newCorrespBin = complete(binUnion, times);
  const timesCount = 0;
  const finalTimes = (newCorrespBin.length / 8) - 1;
  const totalArray = [];
  const segment = segmentation(timesCount, finalTimes,
    newCorrespBin, totalArray);
  const final = segment.map((element) => convertHex(element));
  const output = final.join(' ');
  return output;
}

const output = main();
process.stdout.write(`${ output } \n`);
/**
    node jenniferagve.js
    input:entertaining interpreter
    -------------------------------------------------------------------
    output: B0 9A 89 69 82 60 13 4C 26 A0 2A 2C D4 00
*/

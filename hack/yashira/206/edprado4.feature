Feature: Solve Proyectiles challenge
  from site Yashira
  logged as EdPrado4

Background:
  Given I have access to Internet
  And I have Ubuntu 16.04 LTS OS

Scenario: Challenge solved
  Given A projectile tracking physics exercise
  And A parabolic motion scenario
  When I use the position ecuation to estimate the time
  And I replace the time in a free fall ecuation of the target
  Then I find the exact position of the colision 
  And I use that value as password to solve the challenge


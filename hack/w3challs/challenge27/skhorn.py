#!/usr/bin/env python3

import pdb

class decryption_function_solve():
    """
    You are provided the following encryption function: f(x)=21x+11 [26]
    First, you have to encrypt the word GOOGLE with this function.
    Then, your mission is to find the decryption function g.
    Finally, you have to decrypt the word GELKT.

    Indications:

    1. [26] means "modulo 26".
    2. The decryption function will be of the following form:
       g(y)=ay+b[26], with a and b natural integers lower
       than 26, and y=f(x).
    3. You will enter the answer lowercase on the form
        (crypted word)_(decryption function)_(decrypted word).

    Example:
    If the encryption of GOOGLE was
    BTEVER, that the function was
    g(y)=15y+3[26], and that the decryption of
    GELKT was SUPER, then the answer
    would be: btever_15y+3[26]_super
    """

    def __init__(self):

        plain_text = "GOOGLE"
        chall_ciphertext = "GELKT"
        alpha_values = []
        solve = []

        solve_format_1 = ""
        solve_format_2 = ""

        cipher_text = self.encryption_function(plain_text)
        for i, item in enumerate(cipher_text):
            solve_format_1 += self.alphabet(item)

        print("Solve1:",solve_format_1.lower())

        values = self.find_decryption_function(cipher_text)

        a = values[0]
        b = values[1]
        for i, item in enumerate(chall_ciphertext):
            alpha_values.append(self.alphabet(item))

        output = self.decrypt_function(alpha_values, a, b)
        for i, item in enumerate(output):
            solve += self.alphabet(item)

        solve_format_2 = ''.join(solve)

        print("\n{0}_{1}y+{2}[26]_{3}".format(solve_format_1.lower(), a, b, solve_format_2.lower()))

    def encryption_function(self, plain_text):
        """
        Fn to encrypt the message
        Args:
            plain_text[]    : Plain text to cipher

        Returns:

            cipher_text[]   : Ciphertext
        """
        cipher_text = []
        for i, item in enumerate(plain_text):
            x = int(self.alphabet(item))
            ops = ((21*x)+11)%26
            cipher_text.append(ops)

        return cipher_text

    def find_decryption_function(self, cipher_text):
        """
        Fn to find which are the values a and b used to
        decrypt the plain text, according to this formula
                g(y)=ay+b[26]

        and this statements:
            a < 26
            b < 26
            y = f(x)

        Args:
            cipher_text[]       : An array containing the cipher text

        Returns:
            values[]        : An array containig both a and b values used
                                to decrypt the ciphertext
        """
        output = []
        char = ""

        for a in range(26):
            for b in range(26):
                for i, item in enumerate(cipher_text):

                    decrypt = ((a*item)+b)%26
                    #char += self.alphabet(decrypt)
                    if self.alphabet(decrypt) is None:
                        pass
                    else:
                        char += self.alphabet(decrypt)

                if char == "GOOGLE":
                    print("PlaintText:{0}, a:{1}, b:{2}".format(char, a, b))
                    output.append(a)
                    output.append(b)
                    return output
                else:
                    char = ""

    def decrypt_function(self, cipher_text, a, b):
        """
        For the sake of not missleading the Fn: g(y)=ay+b[26]

        Fn to decrypt the cipher text needed to solve the challenge
        Args:
            cipher_text(str)    : Ciphertext string
            a(int)              : A value
            b(int)              : B value
        """
        plain_text = []
        for i, item in enumerate(cipher_text):
            ops = ((a*item)+b)%26
            plain_text.append(ops)

        return plain_text

    def alphabet(self, char):
        """
        Fn for the sake of simplicity:
            - take a character and get its index value inside the alphabet
            - take a number and get its correspondent character
        Args:
            char(str/int):      Character or number

        Returns:
            The value or the key given the param
        """
        alpha = { \
        'A':0,\
        'B':1,\
        'C':2,\
        'D':3,\
        'E':4,\
        'F':5,\
        'G':6,\
        'H':7,\
        'I':8,\
        'J':9,\
        'K':10,\
        'L':11,\
        'M':12,\
        'N':13,\
        'O':14,\
        'P':15,\
        'Q':16,\
        'R':17,\
        'S':18,\
        'T':19,\
        'U':20,\
        'V':21,\
        'W':22,\
        'X':23,\
        'Y':24,\
        'Z':25 \
        }

        if isinstance(char, int):
            for key, value in alpha.items():
                if value == char:
                    return key

        else:
            return alpha[char]

decryption_function_solve()

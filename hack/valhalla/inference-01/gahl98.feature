## Version 2.0
## language: en

Feature: inference attack valhalla
  Code:
    1
  Site:
    https://halls-of-valhalla.org
  Category:
    inference attack
  User:
    gahl98
  Goal:
    Gathering data without explicit query

  Background:
    Hacker's software:
      | <Software name> | <Version>    |
      | Zorin OS        | 12.4         |
      | Chromium        | 73.0.3683.86 |

  Machine information:
    Given the challenge URL
    """
    https://halls-of-valhalla.org/challenges/inferenceAttacks/inference1.php
    """
    Then I opened the URL with Chromium
    And I see the challenge description
    """
    You may query the database to find some certain statistics such as min,
    max, average, etc. of various data, however queries must include at
    least four users in order to protect each user's privacy.
    Your goal is to determine Alice's salary.
    Note: A real database is used for this challenge,
    and it is rolled back after each attempt.
    """
    And I see a selector with five options
    """
    Average
    Count
    Minimum
    Maximum
    Sum
    """
    And I see another selector with four options
    """
    ID
    Name
    Age
    Salary
    """
    And I see a text box with the label "Where Clause"
    And I see A button with the label "Submit"
    Then I see another text box below ana "Answer:" label
    And I see another "Submit" button

  Scenario: Sucess: aggregate by the sum
    Given the "Aggregate" options
    """
    Average
    Count
    Minimum
    Maximum
    Sum
    """
    Then I think that the count and average can be useful but
    And I think it doesn't let me extrapolate easily the Alice salary
    And I think minimum and maximum neither
    Then I decide the best option to gather the information is by the sum
    And I see the "Column" options
    """
    ID
    Name
    Age
    Salary
    """
    And I think the best option is to get the sum of all salaries
    And I choose the options and write the where clause as follows
    """
    Sum, Salary, ID != 0
    """
    And I hit the "Submit" button
    And it displays and information text
    """
    Result: 8660991
    """
    Then I have to find the sum of salary without Alice
    Then I choose the options and write the where clause as follows
    """
    Sum, Salary, Name != "Alice"
    """
    And I hit the "Submit" button
    And it displays and information text
    """
    Result: 8625564
    """
    Then I make the subtract operation
    And I write in the "Answer" box
    """
    35427
    """
    And I hit the "Submit" button
    And it displays a text that the challenge has been solved
    And I see a resume of how to beat the challenge
    """
    There may be many solutions, but the likely method is to search for the
    sum of all salaries (by selecting 'sum', 'salary', and something like
    '1=1' for where clause) which is 8660991. Then select where
    name!='Alice' which gives 8625564. 8660991-8625564=35427.
    """
    And solved the challenge

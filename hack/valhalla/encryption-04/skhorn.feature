# language: en

Feature: Solve Encryption challenge 4
  From site Valhalla
  From Encryption Category
  With my username Skhorn

  Background:
    Given the fact i have an account on Valhalla site
    And i have Debian 9 as Operating System
    And i have internet access

  Scenario: Succesful Solution
    Given the link to the challenge
    And a description about an encryption technique used in daily life
    And an image containing a barcode
    When i search in google for barcode decoder
    And i download the image
    And upload it into a barcode decoder
    And i see the output is an Upper case english word
    And i use this word as the answer
    Then i solve the challenge

# Version 2.0
## language: en

Feature: Training programming
  Site:
    http://wargame.kr
  Category:
    vulnerability
    hacking
    information disclosure
  User:
    karlhanso82
  Goal:
    find admin password given the site

 Background:
 Hacker's software:
   | <Software name> | <Version>    |
   | Windows OS      | 10           |
   | Chromium        | 74.0.3729.131|

 Machine information:
  Given the challenge URL
  """
  http://wargame.kr:8080/db_is_really_good/index.php
  """
  Then I open the url with Chromium
  And I see the title BLUEMEMO SYSTEM 0.53 beta a login input
  And I type some values in the login input
  Then I supose that must be something related with Sql injection

 Scenario: Success:getting-the-flag
  Then I decided to enter a valid sql injection query
  When I enter the query with the following values
  """
  .test.
  OR 1=1
  """
  And later it give me the following outcome
  """
  _test_
  """
  Then the other one does not give me a useful outcome
  And later i understand that was related with sql code
  Then I decide to enter / in the box
  And it gives me this output
  """
  Fatal error: Uncaught exception 'Exception' with message 'Unable to open
  database: unable to open database file' in /var/www/html/db_is_really_good/
  sqlite3.php:7 Stack trace: #0 /var/www/html/db_is_really_good/sqlite3.php(7)
  : SQLite3->open('./db/wkrm_/.db') #1 /var/www/html/db_is_really_good/
  memo.php(14): MyDB->__construct('./db/wkrm_/.db') #2 {main} thrown in
  /var/www/html/db_is_really_good/sqlite3.php on line 7
  """
  And it shows an error
  Then that the admin file is /db/wrkm_/.db
  And it is a file database and the type is sql3lite
  Then I concluded this file was at
  """
  /var/www/html/db_is_really_good
  """
  Then I got the file typing in the url bar
  """
  http://wargame.kr:8080/db_is_really_good/db/wkrm_admin.db
  """
  And I got the sql file
  Then I opened in hex editor
  And I saw this at the end of the file
  """
  92.168.124.1Congratulations!!
  Here is flag!
  ./dhkdndlswmdzltng.phpS¿£ˆ
  """
  And I Open the url
  Then I got the flag that solves the challenge


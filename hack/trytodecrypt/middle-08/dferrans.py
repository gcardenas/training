"""
$ pylint dferrans.py #linting
--------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
$ flake8 dferrans.py #linting

"""
from __future__ import print_function


def decrypt_middle_8_challenge(data):
    """ Function to decrypt"""

    size = int(data())

    for _ in range(size):
        encrypted_string = input().split()
        dictonary = {"a": "cfk", "b": "cif", "c": "cla", "d": "cnl",
                     "e": "dag", "f": "ddb", "g": "dfm", "h": "dih",
                     "i": "dlc", "j": "dnn", "k": "eai", "l": "edd",
                     "m": "efo", "n": "eij", "o": "ele", "p": "enp",
                     "q": "fak", "r": "fdf", "s": "fga", "t": "fil",
                     "u": "flg", "v": "fob", "w": "gam", "x": "gdh",
                     "y": "gin", "A": "gli", "B": "god", "C": "hao",
                     "D": "hdj", "E": "hge", "F": "hip", "G": "hlk",
                     "H": "hof", "I": "iba", "J": "idl", "K": "igg",
                     "L": "ijb", "M": "ilm", "N": "ioh", "O": "jbc",
                     "P": "jdn", "Q": "jgi", "R": "jjd", "S": "jlo",
                     "T": "joj", "U": "kbe", "V": "kdp", "W": "kgk",
                     "X": "kjf", "Y": "kma", "Z": "kol", "_": "leb",
                     ".": "lgm", ",": "ljh", ";": "lmc", ":": "lon",
                     "?": "mbi", "!": "med", " ": "mgo"}

        inv_map = {v: k for k, v in dictonary.items()}
        result = []
        iterations = len(encrypted_string[0])

        answer = []
        for pairs_str in range(iterations):
            result.append(encrypted_string[0][pairs_str:pairs_str+3])

        for str_to_decrypt in result[0::3]:
            answer.append(inv_map[str_to_decrypt])

        print("".join(answer))


decrypt_middle_8_challenge(input)

# $python dferrans.py < DATA.lst
# keep in mind: its just the middle level

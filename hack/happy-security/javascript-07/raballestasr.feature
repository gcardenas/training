# language: en

Feature: Solve the challenge Just get started 3
  From the happy-security.de website
  From the JavaScript category
  With my username raballestas

  Background:
    Given an input

  Scenario: Successful solution
    When I look into the source code
    Then I see the submit calls an inline javascript function validate()
    When I look into the arguments passed to the function
    Then I see the correct username and password
    And I solve the challenge

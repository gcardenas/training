# language: en

Feature: Solve the challenge Backwards
  From the happy-security.de website
  From the Kryptographie category
  With my username raballestas

  Background:
    Given a string

  Scenario: Successful solution
    When I reverse the string
    Then I see the reversed string contains the password
    And I solve the challenge

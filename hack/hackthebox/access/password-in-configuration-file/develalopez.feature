# language: en

Feature:
  TOE:
    HTB Access Machine
  Page name:
    Hack The Box
  CWE:
    CWE-260: Password in Configuration File
  Goal:
    Getting root.txt flag adn claiming root ownership of the machine.
  Recommendation:
    Disable credential caching.

  Background:
  Hacker's software:
    |    <name>    | <version> |
    | Kali Linux   | 3.30.1    |
    | Firefox ESR  | 60.2.0esr |
    | Burpsuite CE | 1.7.36    |
    | OpenVPN      | 2.4.6     |
  TOE information:
    Given the machine is a standalone server that uses Microsoft IIS 7.5
    And is in Windows Server 2008 R2 x64
    And the machine runs telnet, http, and ftp services.
    And using OpenVPN to access the machine with IP 10.10.10.98

  Scenario: Normal use case
    Given I access the IP
    Then I can see the title LON-MC6
    And an image of servers

  Scenario: Dynamic detection
    Anonymous ftp login is allowed and telnet is open.
    Given that I execute the following nmap command:
    """
    nmap -sC -sV -oA nmap/initial 10.10.10.98
    """
    Then I get the output:
    """
    Nmap scan report for 10.10.10.98
    Host is up (0.19s latency).
    Not shown: 997 filtered ports
    PORT   STATE SERVICE VERSION
    21/tcp open  ftp     Microsoft ftpd
    | ftp-anon: Anonymous FTP login allowed (FTP code 230)
    |_Can't get directory listing: TIMEOUT
    | ftp-syst:
    |_  SYST: Windows_NT
    23/tcp open  telnet?
    80/tcp open  http    Microsoft IIS httpd 7.5
    | http-methods:
    |_  Potentially risky methods: TRACE
    |_http-server-header: Microsoft-IIS/7.5
    |_http-title: MegaCorp
    Service Info: OS: Windows; CPE: cpe:/o:microsoft:windows
    """
    Then I conclude that I can access to information using anonymous FTP login.

  Scenario: Exploitation
  Given that I try to connect to telnet.
  But the service requires username and password.
  Then I connect to FTP with the following command.
  """
  ftp 10.10.10.98
  """
  Then I can inspect the folders
  And download two files: a backup database and a zipped pst file from Outlook.
  When I try to unzip the pst file I find out it has a password.
  Then I look for the password in the Access database file.
  And I find said password in the auth_user table.
  Then I unzip the file using the password assigned to the engineer user.
  And I see an email with the following text in the file:
  """
  Hi there,

  The password for the “security” account has been changed to 4Cc3ssC0ntr0ller.
  Please ensure this is passed on to your engineers.

  Regards,
  John
  """
  Given the obtained credentials in the email
  Then I try and use these credentials in telnet with the following command:
  """
  telnet 10.10.10.98 23 -l security
  """
  And I gain access to the whole server as the security user.
  Then I examine the Users folder and find the Administrator user.
  And I scan the contents of the Users folder with the following command:
  """
  cd && dir root.txt /s /p
  """
  Then I find out that root.txt is in the following path:
  """
  C:\Users\Administrator\Desktop\root.txt
  """
  But I need admin credentials to access it.
  Then I can conclude that I need admin credentials or bypassing the security.

  Scenario: Extraction
  Given That I have access to the telnet service as security.
  Given that have to find the way to read root.txt from the security user.
  Then I execute the following command.
  """
  runas /noprofile /savecred /user:Administrator
  "cmd /V:on /C more C:\Users\Administrator\Desktop\root.txt
  > C:\temp\output.txt"
  """
  And I can read the contents of root.txt from output.txt.
  Then I can access C:\temp\output.txt and read the file.
  Then I can conclude that I succesfully got the root.txt flag
  And now I can claim root ownership of the machine.

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    9.8/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.5/10 (High) - E:U/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    7.5/10 (Medium) - CR:M/IR:L/AR:M

  Scenario: Correlations
  No correlations have been found to this date <18/10/2018>
